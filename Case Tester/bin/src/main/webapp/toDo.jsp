<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/css_toDo.css">

    <title>ToDo Test Cases</title>
  </head>
  <body>
	
	<%
	String[] names = {"Luis","Ernesto","Felipe","Jacki","Nayeli"};
	
	for(int i=0; i<names.length;i++){
	%>
	
	<li><%=names[i]%></li>
	
	<%	
	}
	%>
	
	


    <div class="container">

      <h1>ToDo</h1> <!-- Page Title -->

      <ul class="list-group"> <!-- START Test Case List-->

        <li class="list-group-item "> <!-- List Element-->
          <div class="row">
            <div class="col-md-10">

              <div class="row">
                <p class="toDoListItemTitle">
                  <b>Requirement A001</b>
                </p>
              </div>

              <div class="row">
                <p>
                Due Date: 07/16/2019
                </p>
              </div>

              <div class="row">
                <p>
                This is a brief description. This is a brief description.This is a brief description.
                This is a brief description.This is a brief description.This is a brief description.
                This is a brief description.This is a brief description.This is a brief description.
                </p>
              </div>

            </div>

            <div class="col-md-2">

              <div class="row buttonColumn text-center">
                <div class="col-sm-12">
                  <button type="button" class="btn btn-outline-danger btn-lg btn-block testButton">TEST</button>
                </div>
              </div>

            </div>

            </div>


        </li> <!-- END List Element-->

      </ul> <!-- END Test Case List-->

    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


  </body>
</html>