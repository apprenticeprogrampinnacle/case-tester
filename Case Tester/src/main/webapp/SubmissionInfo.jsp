<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<%
	//Page Tags
	String subTitle, scoreDesc, receivedSubs, detailsH, otherCommentsH;
	int userType = (int)request.getAttribute("userType");
	boolean isChecking = (boolean) request.getAttribute("isChecking");
	String studentName = (String) request.getAttribute("studentName");
	String received = (String) request.getAttribute("received");
	
	
	if(isChecking){
		subTitle = studentName + "'s Submission:";
		scoreDesc = studentName + "'s Score:";
		detailsH = "Detailed FeedBack Analysis";
		otherCommentsH = "Student Comments:";
		
	}else if(userType == 1){
		subTitle = "Key Submission:";
		scoreDesc = "Group average:";
		detailsH = "Detailed Group Feedback";
		otherCommentsH = "Student Comments";
	}else {
		subTitle = "Submission:";
		scoreDesc = "Your score:";
		detailsH = "Detailed Feedback Analysis";
		otherCommentsH = "Instructor Comments";
	}
%>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/SubInfo.css">

    
  </head>
  <body>
	
	<br><br>  
    <div class="container">
    
    	<div class="row">
    		<div class="col-md-12">
	    		<h1><%=subTitle%> ${reqName}</h1>
    		</div>
    	</div>
    	<div class = "row">
	    	<div class="col-md-10">
				<span class="text-secondary">Submitted on: ${subDate}</span>
				
				<%if(userType == 1) { 
				  	if(!isChecking){%>
				  		<br><span class="text-secondary">Received uploads: 15/16</span>
				  	<%} else {%>
				  	<br><br>
				  	
				  	
				  	<button type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-outline-info btn-sm"> + Add Personalized Feedback</button>
				  	
					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					    
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Personalized Feedback</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      
					      <div class="modal-body">
					      	<form name="addPF">
						        <div class="input-group">
								  	<textarea name="TA" class="form-control" aria-label="With textarea"></textarea>
								</div>
							</form>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
					        <form> <!--  method="post" action="addPersFeedback">--> 
               					<input type="button" value="Add" data-dismiss="modal" class="btn btn-outline-primary" onclick="return takeValues()"/>
                			</form>
					      </div>
					      
					    </div>
					  </div>
					</div>
					
					
				<%
					} 
				}
				%>
	    	</div>
    		<div class="col-md-2 text-right">
    			
    				<span class="text-secondary" style="font-size:14px;"><%=scoreDesc%></span><br>
    			
				<span class="score">${score}%</span>
    		</div>
    	</div>
    	<br>
    	
    	<div class="accordion" id="accordionExample">
		  
		  <div class="card">
		    <div class="card-header bg-transparent" id="headingOne">
		      <h2 class="mb-0">
		        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		          <span class="text-secondary"><b><%=detailsH%></b></span>
		        </button>
		      </h2>
		    </div>
		    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		      Number of variables: 5<br>
		      Correct test cases: 16/20
		      </div>
		    </div>
		  </div>
		  
		  <div class="card">
		    <div class="card-header bg-transparent" id="headingTwo">
		      <h2 class="mb-0">
		        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
		          <span class="text-secondary"><b>Your comments</b></span>
		        </button>
		      </h2>
		    </div>
		    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
		      <div class="card-body">
		      	uwu
		      </div>
		    </div>
  		</div>
 		<%if(userType == 0 || isChecking){ %>
	  <div class="card">
	    <div class="card-header bg-transparent" id="headingThree">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
	          <span class="text-secondary"><b><%=otherCommentsH%></b></span>
	        </button>
	      </h2>
	    </div>
	    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
	      <div class="card-body">
	      You suck lol
	      </div>
	    </div>
  	  </div>	  
	<%} %>	  
	</div> <!-- End of accordeon -->
	
	<br><br>
	<div class="row">
	
		<div class="col-md-6">
			<button type="button" class="btn btn-outline-secondary btn-lg">
				Download
			</button>
		</div>
		
		<div class="col-md-6  text-right">
			<button type="button" class="btn btn-outline-primary btn-lg">
				Check on Editor
			</button>
		</div>
	
    </div>

    <!-- Optional JavaScript -->
    <script type="text/javascript">
    
    function takeValues(){
    	var fback =document.forms["addPF"]["TA"].value;
    	
    	if(fback==null||fback==""){
    		alert("No message was written. Click Cancel if you do not want to write a message.");
    		return false;
    	} else{
    		var http = new XMLHttpRequest();
    	    http.open("POST", "addPersFeedback", true);
    	    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    	    var params = "feedback=" + fback; // probably use document.getElementById(...).value
    	    http.send(params);
    	   
    	}
    }
    
    
    </script>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
  </body>
</html>