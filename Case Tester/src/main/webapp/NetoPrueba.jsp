<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      	<!-- Bootstrap CSS -->
      	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<% String tp=request.getParameter("user");
		String pro=request.getParameter("iwant");%>
		<jsp:include page="NavBar.jsp">
			<jsp:param name="user" value="<%= tp %>" />  
		</jsp:include>  
		
		<jsp:include page="/DBser">
			<jsp:param name="iwant" value="<%= pro %>" />  
		</jsp:include>
	</body>
</html>