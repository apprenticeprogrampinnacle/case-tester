<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
</head>
<body>

    <div class="main">

        <h1><img src="image/loginLogo.png" alt="logo" /></h1>
        <div class="container">
            <div class="sign-up-content">
                <form method="POST" class="signup-form" action="addUserServlet">
                    <h2 class="form-title">Sign up</h2>
                    <div class="form-radio">
                        <input type="radio" name="member_level" value="admin" id="admin" checked="checked" />
                        <label for="admin">Admin</label>

                        <input type="radio" name="member_level" value="instructor" id="instructor" />
                        <label for="instructor">Instructor</label>

                        <input type="radio" name="member_level" value="student" id="student" />
                        <label for="student">Student</label>
                    </div>

                    <div class="form-textbox validate-input" data-validate="Name is required">
                        <label for="name">Name(s)</label>
                        <input type="text" name="name" id="name" />
                    </div>

                    <div class="form-textbox validate-input" data-validate="Last name is required">
                        <label for="name">Last name(s)</label>
                        <input type="text" name="lastName" id="lastName" />
                    </div>

                    <div class="form-textbox validate-input" data-validate="Email is required">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" />
                    </div>

                    <div class="form-textbox validate-input" data-validate="Password is required">
                        <label for="pass">Password</label>
                        <input type="password" name="pass" id="pass" />
                    </div>

                    <br></br>
                    
					<%
					
						String errorMsg=(String)request.getAttribute("error");  
						if(errorMsg!=null)
						out.println("<font color=#941b22 size=2px>"+errorMsg+"</font><br></br>");
								
					%>
					

                    <div class="form-textbox">
                        <input type="submit" name="submit" id="submit" class="submit" value="Create account" />
                    </div>
                </form>

            </div>
        </div>

    </div>

    <!-- JS -->
    <script src="js/jquery-add.min.js"></script>
    <script src="js/main-add.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
