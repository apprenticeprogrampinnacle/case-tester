<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<body>
		<%
			String User = request.getParameter("user");
			System.out.println(User);
			if(User.equals("student"))
			{%>		
				<nav class="navbar navbar-expand-md navbar-light sticky-top" style="background-color:  #C9CAC9;">
					<a class="navbar-brand" href="#">
					    <img src="image/Logo.png" height="40px">
					    Case Tester
					</a>
				
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
						</ul>
					
						<UL class="navbar-nav mr-md-5">
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
									<img src="image/NBicon.png" height="40px">
									UserIcon
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="#">Configuration</a>
									<a class="dropdown-item" href="#"></a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="#">Log out</a>
								</div>
							</li>
						</UL>
					</div>
				</nav>
			<%} 
			else if(User.equals("instructor"))
			{ %>
				<nav class="navbar navbar-expand-md navbar-light sticky-top" style="background-color:  #C9CAC9;">
					<a class="navbar-brand" href="#">
						<img src="image/Logo.png" height="40px">
						PeenaKul
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
					aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav mr-auto">
								<li class="nav-item active">
									<a class="nav-link" href="#">Work Summary <span class="sr-only">(current)</span></a>
								</li>
								
								<li class="nav-item active">
									<a class="nav-link" href="#">All users</a>
								</li>
							</ul>
		
							<UL class="navbar-nav mr-md-5">
								<li class="nav-item dropdown ">
									<a class="nav-link" href="#" id="dropdownMenuOffset" role="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false" data-offset="50,100">
										<img src="image/NBbell.png" height="40px">
									</a>
									<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuOffset">
										<a class="dropdown-item" href="#">
											Francisco Gonzalez: 5/10
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">
											Francisco Valenzuela: 5/10
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">
											Francisco Gonzalez: 15/18
										</a>
										<div class="dropdown-divider"></div>
									</div>
								</li>				
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false">
										<img src="image/NBuser.png" height="40px">
										UserIcon
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item" href="#">Configuration</a>
										<a class="dropdown-item" href="#"></a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Log out</a>
									</div>
								</li>
							</UL>
						</div>
					</div>
				</nav>
			<%} 
			else if(User.equals("admin"))
			{ %>
				<nav class="navbar navbar-expand-md navbar-light sticky-top" style="background-color:  #C9CAC9;">
					<a class="navbar-brand" href="#">
						<img src="image/Logo.png" height="40px">
						PeenaKul
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
					aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav mr-auto">
								<li class="nav-item active">
									<a class="nav-link" href="#">All users <span class="sr-only">(current)</span></a>
								</li>
							
								<li class="nav-item">
									<a class="nav-link" href="#">Work Summary</a>
								</li>
							</ul>
		
							<UL class="navbar-nav mr-md-5">
								<li class="nav-item dropdown ">
									<a class="nav-link" href="#" id="dropdownMenuOffset" role="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false" data-offset="50,100">
										<img src="image/NBbell.png" height="40px">
									</a>
									<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuOffset">
										<a class="dropdown-item" href="#">
											Francisco Gonzalez: 5/10
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">
											Francisco Valenzuela: 5/10
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">
											Francisco Gonzalez: 15/18
										</a>
										<div class="dropdown-divider"></div>
									</div>
								</li>				
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false">
										<img src="image/NBuser.png" height="40px">
										UserIcon
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item" href="#">Configuration</a>
										<a class="dropdown-item" href="#"></a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Log out</a>
									</div>
								</li>
							</UL>
						</div>
					</div>
				</nav>
			<%}
			else
			{%>
				<nav class="navbar navbar-expand-md navbar-light sticky-top" style="background-color:  #C9CAC9;">
					<a class="navbar-brand" href="#">
						<img src="image/Logo.png" height="40px">
						PeenaKul
					</a>
				</nav>
			<%}%>
	</body>
</html>