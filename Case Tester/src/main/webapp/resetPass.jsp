<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Reset Password</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	     	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!--===============================================================================================-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

</head>
<body style="background-image: #FFF;">

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="post" action="resetPassServlet">
				
					
					<span class="login100-form-title p-b-43">
						Reset Password
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@pinnacleaerospace.com">
						<input class="input100" type="text" name="email">
						<span class="focus-input100"></span>
						<span class="label-input100">Email</span>
					</div>
					
					 <div class="wrap-input100 validate-input" data-validate="Password is required">
						<input class="input100" type="password" name="newPass">
						<span class="focus-input100"></span>
						<span class="label-input100">New Password</span>
					</div>
					
					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<input class="input100" type="password" name="confPass">
						<span class="focus-input100"></span>
						<span class="label-input100">Confirm Password</span>
					</div>
                    
					<br></br>
		

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" data-toggle="modal" data-target="#myModal">
							Submit
						</button>
					</div>
					
					
					<!-- <div class="loginBox">
						<img src="image/loginLogo.png" class="avatar" alt="Avatar">
					</div>-->
					<br></br>
					<%
					
						String errorMsg=(String)request.getAttribute("error");  
						if(errorMsg!=null)
							out.println("<center><font color=#941b22 size=2px>"+errorMsg+"</font></center>");
						
								
					%>
					
					
				</form>

				<div class="login100-more" style="background-image: url('image/loginBg1.jpg');">
				</div>
				
			</div>
		</div>
	</div>





<!--===============================================================================================-->
	<script src="js/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="js/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/moment.min.js"></script>
	<script src="js/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="js/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
</body>
</html>