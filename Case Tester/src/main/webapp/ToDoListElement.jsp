<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<%
	boolean isSub = (boolean) request.getAttribute("isSubmitted");
	//System.out.println("isSub= "+isSub);
%>

	<li class="list-group-item "> <!-- List Element-->
          <div class="row">
            <div class="col-md-10">
              <div class="row">
                <p class="toDoListItemTitle">
                  <b>${reqName}</b>
                </p>
              </div>
              <div class="row">
                <p>
                Due Date: ${reqDate}
                </p>
              </div>
              <div class="row">
                <p>
                ${reqDesc}
                </p>
              </div>
            </div>

            <div class="col-md-2">
              <div class="row buttonColumn text-center">
                <div class="col-sm-12">
                <%if(!isSub){%>
                	<input type="submit" value="TEST" class="btn btn-outline-danger btn-lg btn-block testButton"/>
                <%
                } else {
                %>
               	<form method="post" action="goToSubmission">
               		<input type="submit" value="Submission" class="btn btn-outline-secondary btn-block btn-lg"/>
                </form>
               	<% 
               	}
               	%>
                </div>
              </div>
            </div>
          </div>
        </li> <!-- END List Element-->

</body>
</html>