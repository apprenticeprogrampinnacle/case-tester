package com.apprentice.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/addPersFeedback")
public class AddPersFeedback extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) {
		String feedback = req.getParameter("feedback");
		System.out.println("received: "+ feedback);
		doGet(req,res);
		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) {
		
		
	}
	

}
