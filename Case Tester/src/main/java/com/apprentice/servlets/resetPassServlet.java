package com.apprentice.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/resetPassServlet")

public class resetPassServlet extends HttpServlet {
		
	    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
	    	
	    	String email = req.getParameter("email");
	    	String newPass = req.getParameter("newPass");
	    	String confPass = req.getParameter("confPass");
	    	
	    	if(newPass.equals(confPass)) {
	    		req.setAttribute("error","Password reseted");
		    	RequestDispatcher rd=req.getRequestDispatcher("/resetPass.jsp");            
		    	rd.include(req, res);
	    	}
	    	else {
	    		req.setAttribute("error","Password does not match");
		    	RequestDispatcher rd=req.getRequestDispatcher("/resetPass.jsp");            
		    	rd.include(req, res);
	    	}

	    }

}
