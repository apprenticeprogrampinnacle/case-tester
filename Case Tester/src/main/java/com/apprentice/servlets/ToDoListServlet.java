package com.apprentice.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/toDoList")
public class ToDoListServlet extends HttpServlet{
	
	private String[] admins = {"Luis", "Nayeli","Ernesto","Jacki","Felipe"};
	
	/*public ToDoListServlet() { //constructor gets called on project startup, not when servlet is triggered
		System.out.println("constructor online");
	}*/
	
	public void init() { //useful to get things done before doGet, usually DB stuff
		//System.out.println("called on request");
		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
			
			ServletContext sc = this.getServletContext();
			RequestDispatcher rd = sc.getRequestDispatcher("/ToDoListElement.jsp");
			
			boolean b = true;
			for (int i = 0; i < 5; i++) {
				b = !b;
				req.setAttribute("reqName","Requirement iWi_0"+i);
				req.setAttribute("reqDate","Jun, 16 2019");
				req.setAttribute("reqDesc","Descripcion aqui");
				req.setAttribute("isSubmitted", b);
			try {
				rd.include(req, res);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}
