package com.apprentice.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/addUserServlet")
public class addUserServlet extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		
		
    	String typeMember = req.getParameter("member_level");
    	String name = req.getParameter("name");
    	String lastName = req.getParameter("lastName");
    	String email = req.getParameter("email");
    	String pass = req.getParameter("pass");
    	
    	if(name!="" && lastName!= "" && email!= "" && pass!= "") {
    		
    		res.sendRedirect("Welcome.jsp");
    		System.out.println(typeMember + " , "+name+" , "+lastName+" , "+email+" , "+pass);
    		
    	}
    	else {
    		req.setAttribute("error","There are some empty inputs");
	    	RequestDispatcher rd=req.getRequestDispatcher("/addNewUser.jsp");            
	    	rd.include(req, res);
    	}
    }

}
