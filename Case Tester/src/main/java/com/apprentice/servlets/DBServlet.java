package com.apprentice.servlets;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.apprentice.casetester.Repo;
import com.apprentice.entities.general.Requirement;
import com.apprentice.entities.general.User;


@WebServlet("/DBser")
public class DBServlet extends HttpServlet
{
	
	@Autowired
	Repo repo;

	public void init() 
	{
		System.out.println("ento a init");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
			String iwant= req.getParameter("iwant");
			if(iwant.equals("names")) name();
			else if(iwant.equals("newuser")) newuser();
			else if(iwant.equals("newtc")) newtc();
			else System.out.println("Chaels men, esa si no te la vengo manejando, ahi para la vuelta primo");
		
	}

	private void newtc() 
	{
		Requirement next = new Requirement();
		next.setReqID("42-IOP1-SRD-7909");
		next.setDueDate("6/5/2019");
		next.setDescript("DCPU4B shall set internal HSDI_Enabled and [B4_op.hsdi_enabled] to TRUE when either internal HSDI_mode is IPS_ON or all of the following are met:\r\n" + 
							"a) internal HSDI_mode is IPS_AUTO\r\n" + 
							"b) internal Pfid_ice_detected_signal is TRUE\r\n" + 
							"c) internal Primary_auto_mode_ena_sig is TRUE ");
		next.setBaseline("xxxx");
		next.setCustomer("xxxx");
		next.setProject("xxxx");
		next.setEcuation("If X||(A&&B&&C) Then Z");
		
		repo.AddTC(next);
	}

	private void newuser() 
	{
		User next = new User();
		next.setName("Johnny");
		next.setLastName("Bravo");
		next.setEmail("Johnny.Bravo@gmail.com");
		next.setPassword("Pinnacle");
		next.setId(11);
		next.setType(1);
		repo.AddU(next);
	}

	private void name() 
	{
		System.out.println(repo.Names());
	}
}


