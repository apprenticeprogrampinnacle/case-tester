package com.apprentice.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/goToSubmission")
public class GoToSubmission extends HttpServlet{
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("wallace");
		ServletContext sc = this.getServletContext();
		RequestDispatcher rd = sc.getRequestDispatcher("/SubmissionInfo.jsp");
		req.setAttribute("reqName", "Requirement_A023");
		req.setAttribute("subDate","June 27, 2019");
		req.setAttribute("score", 98);
		req.setAttribute("userType", 1);
		req.setAttribute("studentName", "Neto");
		req.setAttribute("isChecking", true);
		req.setAttribute("received", "21/21");
		try {
			rd.forward(req, res);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
