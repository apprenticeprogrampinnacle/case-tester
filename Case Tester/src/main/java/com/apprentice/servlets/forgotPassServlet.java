package com.apprentice.servlets;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;

import com.apprentice.entities.testcase_editor.emailSender;

@WebServlet("/forgotPassServlet")
public class forgotPassServlet extends HttpServlet{
	
	private String to;
	private JavaMailSender jms;
	private emailSender emailSend = new emailSender();
	
	public void init() {
		
		
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		String email = req.getParameter("email");
		if(email!="") {
			req.setAttribute("error","An email has been send to Admin.");
	    	RequestDispatcher rd=req.getRequestDispatcher("/forgotPass.jsp");            
	    	rd.include(req, res);
	    	System.out.println(email);
	    	try {
				emailSend.sendEmail();
			} catch (MailException | MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
