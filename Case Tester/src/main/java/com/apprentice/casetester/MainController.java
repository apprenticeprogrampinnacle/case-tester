package com.apprentice.casetester;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class MainController 
{
	
	@GetMapping("/{id}")
	public String General(@PathVariable String id)
	{
		String url=id+".jsp";
		System.out.println(url);
		return url;
	}
	
	@GetMapping("/")
	public String General()
	{
		System.out.println("Sigue jalando");
		return "loginMain.jsp";
	}
	
	//@RequestMapping("toDo")
	public String toDo() {
		return "toDo.jsp";
	}
	
	
}
