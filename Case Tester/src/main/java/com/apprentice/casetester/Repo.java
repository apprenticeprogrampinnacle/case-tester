package com.apprentice.casetester;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.apprentice.entities.general.Requirement;
import com.apprentice.entities.general.User;

@Repository
public class Repo 
{
	@Autowired
	JdbcTemplate jbc;
	
	
	public List<String> Login(String email)
	{
		String search="Select Password from users where Email=\""+email+"\"";
		System.out.println(search);
		List<String> them = new ArrayList<>();
		them.addAll(jbc.queryForList(search, String.class));
		return them;
	}
	
	public List<String> Names()
	{
		String search="Select name from users";
		List<String> them = new ArrayList<>();
		them.addAll(jbc.queryForList(search, String.class));
		return them;
	}
	
	public void AddU(User you)
	{
		String search="INSERT INTO users (User_ID,Name,Lastname,Email) VALUES ('"+you.getId()+"', '"+you.getName()+"', '"+
							you.getLastName()+"', '"+you.getEmail()+"');";
		jbc.execute(search);
		
	}
	
	public void AddTC(Requirement caseT)
	{
		String search="INSERT INTO requirements (Req_ID,Description,Baseline,Customer,Project,Ecuation) VALUES ('"+caseT.getReqID()+"', '"+caseT.getDescript()+"', '"+
				caseT.getBaseline()+"', '"+caseT.getCustomer()+"', '"+caseT.getProject()+"', '"+caseT.getEcuation()+"');";
		jbc.execute(search);
	}
}
