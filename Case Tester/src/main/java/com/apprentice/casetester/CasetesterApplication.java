package com.apprentice.casetester;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan("com.apprentice.servlets")
@SpringBootApplication
public class CasetesterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CasetesterApplication.class, args);
	}

}
