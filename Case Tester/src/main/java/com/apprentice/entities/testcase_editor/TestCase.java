package com.apprentice.entities.testcase_editor;

public class TestCase {

	private String id, coverReq;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCoverReq() {
		return coverReq;
	}

	public void setCoverReq(String coverReq) {
		this.coverReq = coverReq;
	}
	
	
}
