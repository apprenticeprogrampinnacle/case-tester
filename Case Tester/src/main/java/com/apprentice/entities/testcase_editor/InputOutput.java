package com.apprentice.entities.testcase_editor;

import java.util.LinkedList;

public class InputOutput {
	
	private String id;
	private LinkedList<Variable> variables;
	
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public LinkedList<Variable> getVariables() {
		return variables;
	}
	
	public void setVariables(LinkedList<Variable> variables) {
		this.variables = variables;
	}

}
