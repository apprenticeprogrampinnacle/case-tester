package com.apprentice.entities.testcase_editor;

import java.util.HashMap;
import java.util.LinkedList;

public class Variable {
	
	private String name,
				   type;
	private LinkedList<HashMap<String,String>> valueAndNote;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public LinkedList<HashMap<String, String>> getValueAndNote() {
		return valueAndNote;
	}
	public void setValueAndNote(LinkedList<HashMap<String, String>> valueAndNote) {
		this.valueAndNote = valueAndNote;
	}
	
	
	
	
	
	
	
	

}
