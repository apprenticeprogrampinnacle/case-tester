package com.apprentice.entities.testcase_editor;

public class Tuple <X,Y>{
	private X x;
	private Y y;
	
	public Tuple(X x, Y y) {
		this.x = x;
		this.y = y;
	}
	
	public void setFirst(X x) {
		this.x = x;
	}
	
	public X getFirst() {
		return this.x;
	}
	
	public void setSecond(Y y) {
		this.y = y;
	}
	
	public Y getSecond() {
		return this.y;
	}

}
