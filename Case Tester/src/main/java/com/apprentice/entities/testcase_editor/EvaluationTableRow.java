package com.apprentice.entities.testcase_editor;

import java.util.ArrayList;
import java.util.LinkedList;

public class EvaluationTableRow {
	
	private TestCase tC;
	
	private ArrayList<Variable> vars;
	
	private ArrayList<String> out;
	
	private String caseType;

	public TestCase gettC() {
		return tC;
	}

	public void settC(TestCase tC) {
		this.tC = tC;
	}

	public ArrayList<Variable> getVars() {
		return vars;
	}

	public void setVars(ArrayList<Variable> vars) {
		this.vars = vars;
	}

	public ArrayList<String> getOut() {
		return out;
	}

	public void setOut(ArrayList<String> out) {
		this.out = out;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}
	
	
	

}
