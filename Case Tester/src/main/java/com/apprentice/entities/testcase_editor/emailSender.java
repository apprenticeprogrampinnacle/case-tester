package com.apprentice.entities.testcase_editor;

import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class emailSender {
	
	
	@Autowired
	private JavaMailSender javaMailSender;
	private Date sentDate;
	
	
	public void sendEmail() throws MailException, MessagingException{ //should have a user class as parameter
		
		MimeMessage message = javaMailSender.createMimeMessage();
	    MimeMessageHelper helper = new MimeMessageHelper(message, true);//true indicates multipart message
	    helper.setSubject("hello");
	    helper.setTo("nayebeckham@gmail.com");
	    helper.setText("hello", true);//true indicates body is html
	    helper.setFrom("nayeli.beckham@pseinternship.com"); //set sender email and get it from application properties
	    helper.addAttachment("filename", new ClassPathResource("\\static\\path")); //You can add email attachment 
	    javaMailSender.send(message);
	    
	}
	

}
